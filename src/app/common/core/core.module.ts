export interface User {
  uid: string;
  email: string;
  photoURL?: string;
  name: string;
  roleType?:any;
 // phone:string;
}