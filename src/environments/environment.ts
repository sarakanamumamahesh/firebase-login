// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyDkMki6ZcSpgH7j40H3ub4uuHaas-Hs69I",
    authDomain: "fireauth-9381e.firebaseapp.com",
    databaseURL: "https://fireauth-9381e.firebaseio.com",
    projectId: "fireauth-9381e",
    storageBucket: "fireauth-9381e.appspot.com",
    messagingSenderId: "640000320937",
    appId: "1:640000320937:web:b1c1044451b98b4477d0b8",
    measurementId: "G-FBSF9V9G6Q"
  },
  BrandName: 'Newlin Infotech'};
 


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
